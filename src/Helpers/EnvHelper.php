<?php
declare(strict_types=1);

namespace RouteeCom\Helpers;

use RouteeCom\Enum\RouteeComEnvEnum;

class EnvHelper
{
    public static function getRouteeApplicationId():string
    {
        return $_ENV[RouteeComEnvEnum::ROUTEE_APPLICATION_ID]??"";
    }
    public static function setRouteeApplicationId($appId):void
    {
        $_ENV[RouteeComEnvEnum::ROUTEE_APPLICATION_ID] = $appId;
    }

    public static function getRouteeApplicationSecret():string
    {
        return $_ENV[RouteeComEnvEnum::ROUTEE_APPLICATION_SECRET]??"";
    }
    public static function setRouteeApplicationSecret($secret):void
    {
        $_ENV[RouteeComEnvEnum::ROUTEE_APPLICATION_SECRET] = $secret;
    }

    public static function getAuthUrl():string
    {
        return $_ENV[RouteeComEnvEnum::ROUTEE_AUTH_URL]??"";
    }
    public static function setAuthUrl($url):void
    {
        $_ENV[RouteeComEnvEnum::ROUTEE_AUTH_URL] = $url;
    }

    public static function getRouteeApiUrl():string
    {
        return $_ENV[RouteeComEnvEnum::ROUTEE_API_URL]??"";
    }
    public static function setRouteeApiUrl($url):void
    {
        $_ENV[RouteeComEnvEnum::ROUTEE_API_URL] = $url;
    }
}