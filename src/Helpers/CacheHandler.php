<?php
declare(strict_types=1);

namespace RouteeCom\Helpers;

class CacheHandler
{
    public static function set($key, $value, $expiresIn = null)
    {
        $_ENV[$key] = $value;
        if (!empty($expiresIn)){
            $_ENV[$key.'_expiration'] = time() + $expiresIn;
        }
    }

    public static function get($key)
    {
        if (isset($_ENV[$key.'_expiration'])){
            $expiration = $_ENV[$key.'_expiration'];
            if (time() > $expiration){
                return null;
            }
        }
        return $_ENV[$key]??null;
    }
}