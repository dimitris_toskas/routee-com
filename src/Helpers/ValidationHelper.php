<?php
declare(strict_types=1);

namespace RouteeCom\Helpers;

use RouteeCom\Enum\RegexExpressionsEnum;

class ValidationHelper
{
    public static function isValidCountryCode($countryCode):bool
    {
        return (bool)preg_match(RegexExpressionsEnum::COUNTRY_CODE, $countryCode);
    }

    public static function isValidPhonePart($phonePart):bool
    {
        return (bool)preg_match(RegexExpressionsEnum::PHONE_PART, $phonePart);
    }

    public static function isValidPhone($phone):bool
    {
        return (bool)preg_match(RegexExpressionsEnum::PHONE_NUMBER, $phone);
    }

    public static function isEmpty($str):bool
    {
        return empty($str);
    }
}