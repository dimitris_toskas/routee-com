<?php
declare(strict_types=1);

namespace RouteeCom\Entity;

abstract class BaseEntity implements iEntity
{
    protected $propertiesToEntitiesMap = [];
    public function __construct($data = [])
    {
        if (!empty($this->propertiesToEntitiesMap)){
            foreach ($this->propertiesToEntitiesMap as $prop => $entityClass) {
                if (isset($data[$prop])){
                    $this->$prop = new $entityClass($data[$prop]);
                    unset($data[$prop]);
                }
            }
        }
        if (!empty($data)){
            foreach ($data as $key => $value){
                if (property_exists($this, (string)$key)){
                    $this->$key = $value;
                }
            }
        }
    }
}