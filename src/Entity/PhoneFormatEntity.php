<?php
declare(strict_types=1);

namespace RouteeCom\Entity;

class PhoneFormatEntity extends BaseEntity
{
    public $international = null;
    public $national = null;
    public $rfc3966 = null;
}