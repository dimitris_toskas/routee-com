<?php
declare(strict_types=1);

namespace RouteeCom\Entity;

class SendSmsEntity
{
    public $trackingId = null;
    public $status = null;
    public $createdAt = null;
    public $from = null;
    public $to = null;
    public $body = null;
    public $bodyAnalysis = null;
    public $flash = null;

    protected $propertiesToEntitiesMap = [
        'bodyAnalysis'    => BodyAnalysisEntity::class,
    ];
}