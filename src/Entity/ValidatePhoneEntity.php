<?php
declare(strict_types=1);

namespace RouteeCom\Entity;

class ValidatePhoneEntity
{
    public $to = null;
    public $createdAt = null;
    public $getPorted = null;
    public $numberValidatorId = null;
    public $details = null;
    public $formats = null;
    public $geo = null;
    public $applicationName = null;
    public $valid = null;

    protected $propertiesToEntitiesMap = [
        'details'    => PhoneDetailsEntity::class,
        'formats'  => PhoneFormatEntity::class,
        'geo'     => PhoneGeoEntity::class,
    ];
}