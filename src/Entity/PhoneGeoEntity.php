<?php
declare(strict_types=1);

namespace RouteeCom\Entity;

class PhoneGeoEntity extends BaseEntity
{
    public $host = null;
    public $country = null;
    public $countryISO2Code = null;
    public $countryISO3Code = null;
    public $continent = null;
}