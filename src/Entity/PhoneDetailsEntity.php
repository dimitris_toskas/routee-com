<?php
declare(strict_types=1);

namespace RouteeCom\Entity;

class PhoneDetailsEntity extends BaseEntity
{
    public $type = null;
    public $ported = null;
    public $network = null;
    public $networkCode = null;
    public $countryCode = null;
    public $countryPrefix = null;
}