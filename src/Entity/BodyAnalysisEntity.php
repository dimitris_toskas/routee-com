<?php
declare(strict_types=1);

namespace RouteeCom\Entity;

class BodyAnalysisEntity extends BaseEntity
{
    public $parts = null;
    public $unicode = null;
    public $characters = null;
}