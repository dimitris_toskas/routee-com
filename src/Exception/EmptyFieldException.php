<?php
declare(strict_types=1);

namespace RouteeCom\Exception;

use RouteeCom\Enum\RouteComErrorCodeEnum;

class EmptyFieldException extends BaseRouteeComException
{
    public function __construct($fieldname = "")
    {
        $code = RouteComErrorCodeEnum::EMPTY_FIELD;
        $message = RouteComErrorCodeEnum::getMessage($code,['fieldname' => $fieldname]);
        parent::__construct($message, $code);
    }
}