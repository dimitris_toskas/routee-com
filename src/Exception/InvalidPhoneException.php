<?php
declare(strict_types=1);

namespace RouteeCom\Exception;

use RouteeCom\Enum\RouteComErrorCodeEnum;

class InvalidPhoneException extends BaseRouteeComException
{
    public function __construct($message = "", $code = 0, Throwable $previous = null)
    {
        parent::__construct($message, RouteComErrorCodeEnum::INVALID_PHONE_NUMBER, $previous);
    }
}