<?php
declare(strict_types=1);

namespace RouteeCom\Exception;

use RouteeCom\Enum\RouteComErrorCodeEnum;

class BaseRouteeComException extends \Exception
{
    public function __construct($message = "", $code = 0, Throwable $previous = null)
    {
        if (empty($message)){
            $message = RouteComErrorCodeEnum::getMessage($code);
        }
        parent::__construct($message, $code, $previous);
    }
}