<?php
declare(strict_types=1);

namespace RouteeCom\Response;

use RouteeBaseApi\Response\ApiResponse;

class AuthenticationResponse extends ApiResponse
{
    public function getAccessToken()
    {
        $result = $this->getDataFromJson();
        return $result['access_token'] ?? null;
    }
    public function getExpiresIn()
    {
        $result = $this->getDataFromJson();
        return $result['expires_in'] ?? null;
    }
}