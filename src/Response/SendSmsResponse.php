<?php
declare(strict_types=1);

namespace RouteeCom\Response;

use RouteeBaseApi\Response\ApiResponse;
use RouteeCom\Entity\SendSmsEntity;
use RouteeCom\Enum\SmsStatusesEnum;

class SendSmsResponse extends ApiResponse
{
    protected $entityClassToTransform = SendSmsEntity::class;
    public function __construct($response)
    {
        parent::__construct($response);
        if ($this->isSuccess()){
            $smsStatus = $this->getDataFromJson()['status'];
            $this->isSuccess = SmsStatusesEnum::isSuccessStatus($smsStatus);
            $this->message = SmsStatusesEnum::getMessageByStatus($smsStatus);
        }
    }
}