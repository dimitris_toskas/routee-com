<?php
declare(strict_types=1);

namespace RouteeCom\Enum;

class RouteeComEnvEnum
{
    const ROUTEE_APPLICATION_ID = 'ROUTEE_APPLICATION_ID';
    const ROUTEE_APPLICATION_SECRET = 'ROUTEE_APPLICATION_SECRET';
    const ROUTEE_API_URL = 'ROUTEE_API_URL';
    const ROUTEE_AUTH_URL = 'ROUTEE_AUTH_URL';
}