<?php
declare(strict_types=1);

namespace RouteeCom\Enum;

class RouteComErrorCodeEnum
{
    const MISSING_CREDENTIALS  = 8000;
    const MISSING_TOKEN        = 8001;
    const EMPTY_FIELD          = 8002;
    const INVALID_PHONE_NUMBER = 8003;

    public static function getMessage(int $errorCode, array $options = []): string
    {
        $message = '';
        switch ($errorCode) {
            case self::MISSING_CREDENTIALS:
                $message = 'Credentials are missing';
                break;
            case self::MISSING_TOKEN:
                $message = 'Authorization token is missing';
                break;
            case self::EMPTY_FIELD:
                $field = !empty($options['fieldname'])?$options['fieldname']: "";
                $message = 'Field['.$field.'] can not be empty';
                break;
            case self::INVALID_PHONE_NUMBER:
                $message = 'Invalid phone number';
                break;
            default:
                break;
        }
        return $message;
    }
}
