<?php
declare(strict_types=1);

namespace RouteeCom\Enum;

class RegexExpressionsEnum
{
    const COUNTRY_CODE = "/^\+?[0-9]{3}/i";
    const PHONE_PART = "/^\[0-9]{6,12}/i";
    const PHONE_NUMBER = "/^\+?[0-9]{6,15}/i";
}