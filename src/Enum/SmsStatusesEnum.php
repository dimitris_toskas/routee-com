<?php
declare(strict_types=1);

namespace RouteeCom\Enum;

class SmsStatusesEnum
{
    const QUEUED      = 'queued';
    const SENT        = 'sent';
    const DELIVERED   = 'delivered';
    const UNDELIVERED = 'undelivered';
    const FAILED      = 'failed';
    const UNSENT      = 'unsent';

    public static function isSuccessStatus($status):bool
    {
        return strtolower(trim($status)) === self::QUEUED
               || strtolower(trim($status)) === self::SENT
               || strtolower(trim($status)) === self::DELIVERED;
    }

    public static function getMessageByStatus($status):string
    {
        $message = "";
        switch ($status){
            case self::QUEUED:
                $message = 'Request received your SMS is about to be sent.';
                break;
            case self::SENT:
                $message = 'The SMS was sent to its recipient';
                break;
            case self::DELIVERED:
                $message = 'The SMS was sent and it was delivered to its recipient';
                break;
            case self::UNDELIVERED:
                $message = 'Request received but the message was not delivered';
                break;
            case self::FAILED:
                $message = 'The SMS was not sent failed to process it.';
                break;
            case self::UNSENT:
                $message = 'The SMS was not sent due to insufficient balance';
                break;
        }
        return  $message;
    }
}