<?php
declare(strict_types=1);

namespace RouteeCom\Request;

use RouteeBaseApi\Enum\ContentTypeEnum;
use RouteeBaseApi\Enum\GuzzleEnum;
use RouteeBaseApi\Enum\HeadersEnum;
use RouteeBaseApi\Enum\HttpMethodEnum;
use RouteeBaseApi\Response\iResponse;
use RouteeCom\Exception\CredentialsIsMissingException;
use RouteeCom\Helpers\PasswordHelper;
use RouteeCom\Response\AuthenticationResponse;

class AuthenticationRequest extends BaseRouteComRequest
{
    public function __construct($url = null, $method = HttpMethodEnum::GET, $params = [], $headers = [])
    {
        $params['grant_type'] = 'client_credentials';
        $url = "https://auth.routee.net/";
        $this->setParamsType(GuzzleEnum::FORM_PARAMS);
        $this->setAction('oauth/token');
        $this->setResponseType(AuthenticationResponse::class);
        parent::__construct($url, HttpMethodEnum::POST, $params, $headers);
    }

    public function setCredentials($applicationId, $applicationSecret)
    {
        $this->setAuth($applicationId, $applicationSecret);
    }

    /**
     * @throws \RouteeBaseApi\Exception\MissingRequiredFieldException
     * @throws CredentialsIsMissingException
     * @throws \RouteeBaseApi\Exception\MissingActionException
     */
    protected function validateRequest():void
    {
        if (empty($this->getAuth())){
            throw new CredentialsIsMissingException();
        }
        parent::validateRequest();
    }

    public function execute(): iResponse
    {
        unset($this->headers[HeadersEnum::AUTHORIZATION]);
        unset($this->headers[HeadersEnum::CONTENT_TYPE]);
        return parent::execute();
    }
}