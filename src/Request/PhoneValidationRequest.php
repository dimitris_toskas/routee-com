<?php
declare(strict_types=1);

namespace RouteeCom\Request;

use RouteeBaseApi\Enum\HttpMethodEnum;

class PhoneValidationRequest extends AuthenticatedRequest
{
    public function __construct($url = null, $method = HttpMethodEnum::GET, $params = [], $headers = [])
    {
        $this->setAction("numbervalidator");
        $this->setRequiredParams(['to']);
        parent::__construct($url, HttpMethodEnum::POST, $params, $headers);
    }
}