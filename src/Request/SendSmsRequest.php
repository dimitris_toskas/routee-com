<?php
declare(strict_types=1);

namespace RouteeCom\Request;

use GuzzleHttp\Exception\GuzzleException;
use RouteeBaseApi\Enum\HttpMethodEnum;
use RouteeBaseApi\Response\ApiResponse;
use RouteeBaseApi\Response\iResponse;
use RouteeCom\Enum\SmsStatusesEnum;
use RouteeCom\Exception\EmptyFieldException;
use RouteeCom\Exception\InvalidPhoneException;
use RouteeCom\Helpers\ValidationHelper;
use RouteeCom\Response\SendSmsResponse;

class SendSmsRequest extends AuthenticatedRequest
{
    public function __construct($url = null, $method = HttpMethodEnum::GET, $params = [], $headers = [])
    {
        $this->setRequiredParams(['from','to','body']);
        $this->setAction('sms');
        $this->setResponseType(SendSmsResponse::class);
        $this->setParamsType('json');
        parent::__construct($url, HttpMethodEnum::POST, $params, $headers);
    }

    /**
     * @throws GuzzleException
     */
    public function send($from, $to, $body): iResponse
    {
        try {
            $this->setSmsInfo($from, $to, $body);
            return $this->execute();
        }
        catch (\Exception $ex){
            return new ApiResponse($ex);
        }
    }

    /**
     * @throws InvalidPhoneException
     * @throws EmptyFieldException
     */
    public function setSmsInfo($from, $to, $body):void
    {
        if (ValidationHelper::isEmpty($from)){
            throw new EmptyFieldException("from");
        }
        if (ValidationHelper::isEmpty($body)){
            throw new EmptyFieldException("body");
        }
        //@todo[Dimitris] Check if has permission and has enable the feature to use PhoneValidation request
        if (!ValidationHelper::isValidPhone($to)){
            throw new InvalidPhoneException();
        }
        $this->setParams([
           'from' => $from,
           'to' => $to,
           'body' => $body,
        ]);
    }
}