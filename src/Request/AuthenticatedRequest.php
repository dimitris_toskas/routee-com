<?php
declare(strict_types=1);

namespace RouteeCom\Request;

use RouteeBaseApi\Enum\HeadersEnum;
use RouteeBaseApi\Enum\HttpMethodEnum;
use RouteeBaseApi\Response\iResponse;
use RouteeCom\Exception\AuthenticationTokesIsMissing;
use RouteeCom\Helpers\CacheHandler;
use RouteeCom\Helpers\EnvHelper;

abstract class AuthenticatedRequest extends BaseRouteComRequest
{
    protected $accessToken = null;

    /**
     * @throws \RouteeBaseApi\Exception\MissingRequiredFieldException
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws AuthenticationTokesIsMissing
     * @throws \RouteeBaseApi\Exception\MissingActionException
     */
    public function __construct($url = null, $method = HttpMethodEnum::GET, $params = [], $headers = [])
    {
        $this->login();
        parent::__construct($url, $method, $params, $headers);
    }

    public function execute(): iResponse
    {
        $this->headers[HeadersEnum::AUTHORIZATION] = 'Bearer '.$this->accessToken;
        return parent::execute();
    }

    /**
     * @throws \RouteeBaseApi\Exception\MissingRequiredFieldException
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \RouteeBaseApi\Exception\MissingActionException
     * @throws AuthenticationTokesIsMissing
     */
    protected function login(){
        if (empty($this->accessToken)){
            if (CacheHandler::get('auth')){
                $this->accessToken = CacheHandler::get('auth');
            }
            else {
                if (!empty(EnvHelper::getRouteeApplicationId())
                    && !empty(EnvHelper::getRouteeApplicationSecret())
                ){
                    $request = new AuthenticationRequest();
                    $request->setAuth(EnvHelper::getRouteeApplicationId(), EnvHelper::getRouteeApplicationSecret());
                    $response = $request->execute();
                    $this->accessToken = $response->getAccessToken();
                    CacheHandler::set('authToken', $this->accessToken, $response->getExpiresIn());
                }
                else {
                    throw new AuthenticationTokesIsMissing();
                }
            }
        }
        return $this->accessToken;
    }
}