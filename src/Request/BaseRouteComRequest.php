<?php
declare(strict_types=1);

namespace RouteeCom\Request;

use RouteeBaseApi\Request\BaseRequest;
use RouteeCom\Helpers\EnvHelper;

class BaseRouteComRequest extends BaseRequest
{
    private $token = null;
    public function __construct($url = null, $method = \RouteeBaseApi\Enum\HttpMethodEnum::GET, $params = [], $headers = [])
    {
        if (empty($url)){
            $url = !empty(EnvHelper::getRouteeApiUrl())
                ?EnvHelper::getRouteeApiUrl()
                :"https://connect.routee.net/";
        }
        parent::__construct($url, $method, $params, $headers);
    }
    protected function validateRequest()
    {

    }
}