<?php
declare(strict_types=1);


use PHPUnit\Framework\TestCase;
use RouteeCom\Entity\SendSmsEntity;
use RouteeCom\Exception\CredentialsIsMissingException;
use RouteeCom\Helpers\EnvHelper;
use RouteeCom\Request\AuthenticationRequest;
use RouteeCom\Request\SendSmsRequest;

class SendSmsRequestTest extends TestCase
{
    /**
     * @var SendSmsRequest|null
     */
    protected $request = null;
    protected function setUp():void
    {
        parent::setUp();
        EnvHelper::setRouteeApplicationId("5f9138288b71de3617a87cd3");
        EnvHelper::setRouteeApplicationSecret("RSj69jLowJ");
        $this->request = new SendSmsRequest();
    }
    public function testSendSms()
    {
        $response = $this->request->send('Dimitris','+35799138271','This is a test');
        $this->assertTrue($response->isSuccess());
    }
    public function testEmptyFrom()
    {
        $response = $this->request->send('','+35799138271','This is a test');
        $this->assertFalse($response->isSuccess());
        $this->assertEquals('Field[from] can not be empty',$response->getMessage());
    }
    public function testEmptyBody()
    {
        $response = $this->request->send('Dimitris','+35799138271','');
        $this->assertFalse($response->isSuccess());
        $this->assertEquals('Field[body] can not be empty',$response->getMessage());
    }
    public function testInvalidPhone()
    {
        $response = $this->request->send('Dimitris','+00x00','This is a test');
        $this->assertFalse($response->isSuccess());
        $this->assertEquals('Invalid phone number',$response->getMessage());
    }
    public function testSendSmsEntity()
    {
        $response = $this->request->send('Dimitris','+35799138271','This is a test');
        $this->assertTrue($response->isSuccess());
        $entity = $response->toEntity();
        $this->assertInstanceOf(SendSmsEntity::class, $entity);
    }
}