<?php
declare(strict_types=1);


use PHPUnit\Framework\TestCase;
use RouteeCom\Exception\CredentialsIsMissingException;
use RouteeCom\Request\AuthenticationRequest;

class AuthenticationRequestTest extends TestCase
{
    public function testAuthenticationRequest()
    {
        $applicationId = '5f9138288b71de3617a87cd3';
        $applicationSecret = 'RSj69jLowJ';
        $authRequest = new AuthenticationRequest();
        $authRequest->setCredentials($applicationId, $applicationSecret);
        $authResponse = $authRequest->execute();
        $this->assertTrue($authResponse->isSuccess());
        $this->assertNotEmpty($authResponse->getAccessToken());
        $this->assertNotEmpty($authResponse->getExpiresIn());
    }

    public function testFailAuthentication()
    {
        $applicationId = 'appid';
        $applicationSecret = 'appsecret';
        $authRequest = new AuthenticationRequest();
        $authRequest->setCredentials($applicationId, $applicationSecret);
        $authResponse = $authRequest->execute();
        $this->assertFalse($authResponse->isSuccess());
    }

    public function testMissingCredentials()
    {
        $this->expectException(CredentialsIsMissingException::class);
        $authRequest = new AuthenticationRequest();
        $authResponse = $authRequest->execute();
    }
}